$(document).ready(function(){

  $('#btn-send-message').on('click', function(){
    var nombre = $('#nombre').val();
    var email = $('#email').val();
    var telefono = $('#telefono').val();
    var paquetes = $('#paquetes').val();
    var motivo = $('#motivo').val();
    var rubros = $('#rubros').val();
    var mensaje = $('#mensaje').val();

    if(nombre =='' && email != ''){
      $('.help-block').css('color', '#c94d00');
      $('.help-nombre').empty();
      $('.help-nombre').append('<ul><li>Este campo es obligatorio</li></ul>');
      return false;
    }

    if(email == '' && nombre != ''){
      $('.help-block').css('color', '#c94d00');
      $('.help-email').empty();
      $('.help-email').append('<ul><li>Este campo es obligatorio</li></ul>');
      return false;
    }

    if(email == '' && nombre == ''){
      $('.help-block').css('color', '#c94d00');
      $('.help-email').empty();
      $('.help-email').append('<ul><li>Este campo es obligatorio</li></ul>');
      $('.help-nombre').empty();
      $('.help-nombre').append('<ul><li>Este campo es obligatorio</li></ul>');
      return false;
    }

    if($('.help-email').text() != ''){
      $('.help-block').css('color', '#c94d00');
      return false;
    }

    var url = "send.php";
    console.log($('#contact-form').serialize());
    $.ajax({
        type: "POST",
        url: url,
        data: $('#contact-form').serialize(),
        success: function (data)
        {
          if(data.status == true){
            console.log(data);
            $('#mesagge-response-contact').empty();
            $('#mesagge-response-contact').append('<div class="alert alert-success alert-dismissible" role="alert">'+
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
              data.message_send+
            '</div>').fadeOut(10000);
          }
          else if(data.status == false){
            $('#mesagge-response-contact').empty();
            $('#mesagge-response-contact').append('<div class="alert alert-danger alert-dismissible" role="alert">'+
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
              data.message_send+
            '</div>').fadeOut(10000);
            console.log(data.error);
          }
        }
    });
    return false;

  });

});
