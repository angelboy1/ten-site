<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load composer's autoloader
require 'vendor/autoload.php';
header('Content-type: application/json; charset=utf-8');

$jsondata = [];
$nombre = $_POST['nombre'];
$email = $_POST['email'];
$telefono = $_POST['telefono'];
$rubros = $_POST['rubros'];
$paquetes = $_POST['paquetes'];
$motivo = $_POST['motivo'];
$mensaje = $_POST['mensaje'];

$mail = new PHPMailer;
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = "email-smtp.us-west-2.amazonaws.com";  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'AKIAIBUK36KCFZ7LKTRA';                 // SMTP username
$mail->Password = 'AqtnZTt1gc53nroJy6ejFulw2jktowkIeP7kMLT57fF6';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

$mail->setFrom('allenvalencia@ten.pe', ucwords(strtolower($_POST['nombre'])));
// Add a recipient
$mail->addAddress('avalencia.ten@gmail.com', 'Allen Valencia');               // Name is optional
//$mail->addAddress('angeliusboy24@gmail.com', 'Allen Valencia');

$mail->isHTML(true);                                  // Set email format to HTML

$cuerpo = '<strong>Nombre: </strong>'.$nombre.'<br><strong>E-mail: </strong>'.$email.'<br><strong>Teléfono: </strong>'.$telefono.'<br><strong>Rubros: </strong>'.rubro($rubros).'<br><strong>Paquetes: </strong>'.paquete($paquetes).'<br><strong>Motivo: </strong>'.motivo($motivo).'<br><strong>Mensaje: </strong>'.$mensaje;

$mail->Subject = 'Contacto TEN';
$mail->Body    = $cuerpo;
$mail->AltBody = $nombre.' / '.$email;

if(!$mail->send()) {
  $jsondata = [
                'status' => false,
                'message_send' => '<strong style="text-transform: uppercase;">Ocurrio un error en el envio</strong>',
                'error' => $mail->ErrorInfo
              ];
} else {
  $jsondata = [
                'nombre' => $nombre,
                'email' => $email,
                'telefono' => $telefono,
                'rubros' => $rubros,
                'paquetes' => $paquetes,
                'motivo' => $motivo,
                'mensaje' => $mensaje,
                'status' => true,
                'message_send' => '<strong style="text-transform: uppercase;"> Gracias por escribirnos '.$nombre.'!</strong> Tu mensaje ha sido enviado a nuestra central, en breves momentos nos estaremos comunicando contigo para atender tu consulta.'
              ];
}

echo json_encode($jsondata);
exit();

function rubro($rubro){
  switch ($rubro) {
        case 1:
          return 'Moda y belleza';
        break;
        case 2:
          return 'Eventos y entretenimiento';
        break;
        case 3:
          return 'Comidas y bebidas';
        break;
        case 4:
          return 'Decoración';
        break;
        case 5:
          return 'Negocios locales';
        break;
        case 6:
          return 'Animales';
        break;
        case 7:
          return 'Bienestar y salud';
        break;
        case 8:
          return 'Deporte';
        break;
        case 9:
          return 'Otros';
        break;
        default:
          return '';
        break;
 }
}

function paquete($paquete){
  switch ($paquete) {
        case 1:
          return 'Plan Starter';
        break;
        case 2:
          return 'Plan Premium';
        break;
        case 3:
          return 'Necesito más información';
        break;
        default:
          return '';
        break;
    }
 }

function motivo($motivo){
   switch ($motivo) {
         case 1:
           return 'Tienda virtual';
         break;
         case 2:
           return 'App';
         break;
         case 3:
           return 'Otros';
         break;
         default:
           return '';
         break;
    }
}


?>
